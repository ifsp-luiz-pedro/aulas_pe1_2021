// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebaseConfig: {
        apiKey: "AIzaSyAU7-PtFaMhLY3qRyIe6in2avEJbBxAMyE",
        authDomain: "controle-ifspguarulhos.firebaseapp.com",
        projectId: "controle-ifspguarulhos",
        storageBucket: "controle-ifspguarulhos.appspot.com",
        messagingSenderId: "699166991438",
        appId: "1:699166991438:web:4d72772310a4ea7a8e8f1f",
        measurementId: "G-8CG4YWNKCK"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
