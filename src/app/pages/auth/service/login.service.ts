import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { NavController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  isUserAuthenticated: Observable<any>;

  constructor(
    private nav: NavController,
    private auth: AngularFireAuth,
    private toast: ToastController,
  ) {
    this.isUserAuthenticated = auth.authState;
   }

  private async showError(){
    const ctrl = await this.toast.create({
      message: 'Dados de acesso incorretos',
      duration: 3000
    });

    ctrl.present();
  }

  login(user){
    this.auth.signInWithEmailAndPassword(user.email, user.password)
    .then(() => this.nav.navigateForward('home'))
    .catch(() => this.showError());
  }

  recoverPass(data){
    this.auth.sendPasswordResetEmail(data.email)
    .then(() => this.nav.navigateBack('auth'))
    .catch(err => console.log(err));
  }

  createUser(user){
    this.auth.createUserWithEmailAndPassword(user.email, user.password)
    .then(credentials => console.log(credentials));
  }

  logout(){
    this.auth.signOut()
    .then(() => this.nav.navigateBack('auth'));
  }

}

